package com.steganographypoc.bitmap


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Environment
import android.util.Log
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.bridge.ReadableArray
import com.facebook.react.bridge.WritableNativeArray
import java.io.File
import java.io.FileOutputStream
import java.lang.Math
import java.net.URLConnection
import java.io.FileInputStream
import android.provider.MediaStore
class BitmapModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
    override fun getName(): String {
        return "Bitmap"
    }
    private fun saveImage(bitmap: Bitmap, filePath: String) {
        val file = File(filePath)
        val outStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
        outStream.close()
    }

    @OptIn(ExperimentalStdlibApi::class)
    private fun embedText(bitmap: Bitmap, text: String, filePath: String): Bitmap {
        val bitMask = 0x00000001 // define the mask bit used to get the digit
        var bit: Int // define an integer number to represent the ASCII number of a character
        var x = 0 // define the starting pixel x
        var y = 0 // define the starting pixel y
        for (i in text.indices) {
            bit = text[i].code // get the ASCII number of a character
            for (j in 0..<8) {
                val flag = bit and bitMask // get 1 digit from the character
                if (flag == 1) {
                    if (x < bitmap.width) {
                        bitmap.setPixel(x, y, bitmap.getPixel(x, y) or 0x00000001) // store the bit which is 1 into a pixel's last digit
                        x++
                    } else {
                        x = 0
                        y++
                        bitmap.setPixel(x, y, bitmap.getPixel(x, y) or 0x00000001) // store the bit which is 1 into a pixel's last digit
                    }
                } else {
                    if (x < bitmap.width) {
                        bitmap.setPixel(x, y, bitmap.getPixel(x, y) and -0x2) // store the bit which is 0 into a pixel's last digit
                        x++
                    } else {
                        x = 0
                        y++
                        bitmap.setPixel(x, y, bitmap.getPixel(x, y) and -0x2) // store the bit which is 0 into a pixel's last digit
                    }
                }
                bit = bit shr 1 // get the next digit from the character
            }
        }
        return bitmap
    }
    @ReactMethod
    fun convert(filePath: String, secret: String, promise: Promise){
        val myDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Stegappasaurus")
        if (!myDir.exists()) {
            if (!myDir.mkdirs()) {
                promise.reject("failed_create_folder", "failed to create directory")
            }
        }

        val date = System.currentTimeMillis()
        val fname = "$date.png"
        val outputPath ="$myDir$fname"
        val originalBitmap = BitmapFactory.decodeFile(filePath)
        val modifiedBitmap = embedText(originalBitmap.copy(originalBitmap.config, true), "Secret", outputPath)
        saveImage(modifiedBitmap, outputPath)

        promise.resolve(outputPath)
    }


    @ReactMethod
    fun getPixels(filePath: String, bitsRequired: Int, promise: Promise) {
        try {

            val bitmap = this.getBitmap(filePath)
            val pixelsRequired = Math.ceil(bitsRequired / 3.0).toInt()
            val width = bitmap.getWidth()
            val height = bitmap.getHeight()

            val requiredWidth = pixelsRequired % width
            val requiredHeight = pixelsRequired.div(height) + 1

            val pixels = WritableNativeArray()
            for (x in 0 until requiredWidth) {
                for (y in 0 until requiredHeight) {
                    val color = bitmap.getPixel(x, y)
                    pixels.pushInt(Color.red(color))
                    pixels.pushInt(Color.green(color))
                    pixels.pushInt(Color.blue(color))
                }
            }

            promise.resolve(pixels)
        } catch (e: Exception) {
            promise.reject(e)
        }
    }

    @ReactMethod
    fun setPixels(filePath: String, pixels: ReadableArray, promise: Promise) {
        try {

            val bitmap = this.getBitmap(filePath)
            val width = bitmap.getWidth()
            val height = bitmap.getHeight()
            val pixelsRequired = pixels.size().div(3)

            for (i in 0 until pixelsRequired) {
                val color = Color.argb(255, pixels.getInt(i * 3), pixels.getInt(i * 3 + 1), pixels.getInt(i * 3 + 2))
                val x = i % width
                val y = i.toInt().div(height)
                bitmap.setPixel(x, y, color)
            }

            val myDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Stegappasaurus")
            if (!myDir.exists()) {
                if (!myDir.mkdirs()) {
                    promise.reject("failed_create_folder", "failed to create directory")
                }
            }

            val date = System.currentTimeMillis()
            val fname = "$date.png"
            val file = File(myDir, fname)
            val out = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)

            out.flush()
            out.close()
            val uri = Uri.fromFile(file)
            promise.resolve(uri.toString())
        } catch (e: Exception) {
            promise.reject(e)
        }
    }

    fun getBitmap(filePath: String): Bitmap {
        val context = getReactApplicationContext()
        val cr = context.getContentResolver()
        val uri = Uri.parse(filePath)

        val bitmap: Bitmap
        if (android.os.Build.VERSION.SDK_INT >= 28){
            val source = ImageDecoder.createSource(cr, uri)
            val onHeaderListener = ImageDecoder.OnHeaderDecodedListener { decoder, info, source ->
                decoder.setMutableRequired(true)
            }
            bitmap = ImageDecoder.decodeBitmap(source, onHeaderListener)
        }
        else {
            bitmap = MediaStore.Images.Media.getBitmap(cr, uri)
        }

        return bitmap
    }
}
