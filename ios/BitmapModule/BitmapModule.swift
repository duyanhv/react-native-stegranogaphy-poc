import Foundation
import UIKit
import Photos
import React
@objc(BitmapModule)
class BitmapModule: NSObject {
    
    @objc(getPixels:bitsRequired:resolver:rejecter:)
    func getPixels(filePath: String, bitsRequired: Int, resolve:RCTPromiseResolveBlock, reject:RCTPromiseRejectBlock) -> Void {
        do {
            let image = try self.getImage(filePath: filePath)
            let pixelsRequired = Int(ceil(Double(bitsRequired) / 3.0))
            let width = image.size.width
            let height = image.size.height
            
            let requiredWidth = pixelsRequired % Int(width)
            let requiredHeight = pixelsRequired / Int(height) + 1
            
            var pixels: [Int] = []
            for x in 0..<requiredWidth {
                for y in 0..<requiredHeight {
                    let pixelData = image.cgImage!.dataProvider!.data
                    let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
                    
                    let pixelInfo: Int = ((Int(image.size.width) * Int(y)) + Int(x)) * 4
                    
                    let r = Int(data[pixelInfo])
                    let g = Int(data[pixelInfo+1])
                    let b = Int(data[pixelInfo+2])
                    
                    pixels.append(contentsOf: [r, g, b])
                }
            }
            
            resolve(pixels)
        } catch {
            reject("get_pixels_error", "Error getting pixels", error)
        }
    }
    
    @objc(setPixels:filePath:resolver:rejecter:)
    func setPixels(pixels: [Int], filePath: String, resolve:RCTPromiseResolveBlock, reject:RCTPromiseRejectBlock) -> Void {
        do {
            let image = try self.getImage(filePath: filePath)
            let width = image.size.width
            let height = image.size.height
            let pixelsRequired = pixels.count / 3
            
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
            
            let bitsPerComponent = 8
            let bytesPerRow = 4 * Int(width)
            
            var newPixels = pixels
            
            let context = CGContext(data: &newPixels, width: Int(width), height: Int(height), bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
            
            let newImage = context.makeImage()!
            let finalImage = UIImage(cgImage: newImage)
            
            // Save the image
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileName = "\(Date().timeIntervalSince1970).png"
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            
            if let data = finalImage.pngData() {
                try data.write(to: fileURL)
                resolve(fileURL.absoluteString)
            } else {
                reject("save_error", "Error saving image", nil)
            }
        } catch {
            reject("set_pixels_error", "Error setting pixels", error)
        }
    }
    
    private func getImage(filePath: String) throws -> UIImage {
        guard let url = URL(string: filePath) else {
            throw NSError(domain: "Invalid file path", code: 1)
        }
        
        let data = try Data(contentsOf: url)
        guard let image = UIImage(data: data) else {
            throw NSError(domain: "Invalid image data", code: 2)
        }
        
        return image
    }
}
