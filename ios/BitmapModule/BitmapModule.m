#import "BitmapModule.h"
#import <React/RCTLog.h>
#import <Photos/Photos.h>
#import <UIKit/UIKit.h>
@implementation BitmapModule

RCT_EXPORT_MODULE(Bitmap);
RCT_EXPORT_METHOD(getPixels:(NSString *)filePath
                  bitsRequired:(NSInteger)bitsRequired
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
  
    if ([filePath hasPrefix:@"file://"]) {
         filePath = [filePath substringFromIndex:7];
     }
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
  
    if (!image) {
        reject(@"image_error", @"Could not load image", nil);
        return;
    }
      CGImageRef cgImage = image.CGImage;
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    const UInt8 *imageDataPtr = CFDataGetBytePtr(imageData);

    NSUInteger width = CGImageGetWidth(cgImage);
    NSUInteger height = CGImageGetHeight(cgImage);

    NSInteger pixelsRequired = ceil(bitsRequired / 3.0);
    NSInteger requiredWidth = pixelsRequired % width;
    NSInteger requiredHeight = (NSInteger)ceil(pixelsRequired / (double)height);

    NSMutableArray *pixels = [NSMutableArray new];
    for (NSInteger x = 0; x < requiredWidth; x++) {
        for (NSInteger y = 0; y < requiredHeight; y++) {
            // Calculate pixel offset
            NSInteger offset = (y * width + x) * 4; // Assuming RGBA

            // Extract RGB (Handle alpha appropriately if needed)
            UInt8 red = imageDataPtr[offset];
            UInt8 green = imageDataPtr[offset + 1];
            UInt8 blue = imageDataPtr[offset + 2];
            
            [pixels addObject:@(red)];
            [pixels addObject:@(green)];
            [pixels addObject:@(blue)];
        }
    }

    CFRelease(imageData);
    resolve(pixels);
}
RCT_EXPORT_METHOD(setPixels:(NSString *)filePath
                  pixels:(NSArray *)pixels
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
  if ([filePath hasPrefix:@"file://"]) {
       filePath = [filePath substringFromIndex:7];
   }
    // Get the existing image
    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
    if (!image) {
      reject(@"image_error", @"Could not load image", nil);
      return;
    }

    // Get image data for modification
    CGImageRef cgImage = image.CGImage;
    NSUInteger width = CGImageGetWidth(cgImage);
    NSUInteger height = CGImageGetHeight(cgImage);

    // Ensure enough pixels are provided
    NSInteger pixelsRequired = pixels.count / 3;
    if (pixelsRequired > width * height){
        reject(@"pixel_error", @"Not enough pixels provided", nil);
        return;
    }

    // Create context to modify pixels
    size_t bytesPerRow = width * 4; // Assuming RGBA
    UInt8 *pixelData = malloc(width * height * 4);
    CGContextRef context = CGBitmapContextCreate(pixelData, width, height, 8, bytesPerRow, CGImageGetColorSpace(cgImage), kCGImageAlphaPremultipliedLast);

    // Apply pixel changes
  for (NSInteger i = 0; i < pixelsRequired; i++) {
    NSInteger x = i % width;
           NSInteger y = i / width;
           NSInteger offset = (y * width + x) * 4;

           CGFloat red = [pixels[i * 3] doubleValue];
           CGFloat green = [pixels[i * 3 + 1] doubleValue];
           CGFloat blue = [pixels[i * 3 + 2] doubleValue];
          CGFloat alpha =  255;
          pixelData[offset] = red;
          pixelData[offset + 1] = green;
          pixelData[offset + 2] = blue;
          pixelData[offset + 3] = alpha;


   }

    // Create a new UIImage from altered pixel data
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];

    // Release image data
    CGContextRelease(context);
    free(pixelData);
    CGImageRelease(newImageRef);

  NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSPicturesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:@"Stegappasaurus"];

  // Ensure the folder exists
  [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];

  // Generate filename
  NSString *fileName = [NSString stringWithFormat:@"%lld.png", (long long)[NSDate date].timeIntervalSince1970];

  // Construct full path to the file inside the folder
  NSString *imagePath = [folderPath stringByAppendingPathComponent:fileName];
  [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
      if (status == PHAuthorizationStatusAuthorized) {
         
        NSData *imageData = UIImagePNGRepresentation(newImage);
        if (![imageData writeToFile:imagePath atomically:YES]) {
            reject(@"save_error", @"Failed to save image", nil);
            return;
        }

        NSURL *fileURL = [NSURL fileURLWithPath:imagePath];
        NSString *uriString = [fileURL absoluteString];
        resolve(uriString);
      } else {
          // Permission denied. Handle this situation, e.g., by informing the user
          reject(@"permissions_error", @"Photo library access denied", nil);
      }
  }];

}


@end
