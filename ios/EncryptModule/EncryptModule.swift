//
//  EncryptModule.swift
//  steganographyPoc
//
//  Created by Duy Anh Vu on 24/4/24.
//

import Foundation
import UIKit


@objc(EncryptModule)
class EncryptModule: NSObject {
  @objc func embedText(_ imagePath: String, text: String, resolver resolve: @escaping RCTPromiseResolveBlock, rejecter reject: @escaping RCTPromiseRejectBlock) -> Void {
    
    print("myVariable: \(text)")
    let fileURL = URL(string: imagePath)
    if let filePath = fileURL?.path {
      guard var image = UIImage(contentsOfFile: filePath) else {
                 reject("E_LOAD", "Failed to load image from path: \(filePath)", nil)
                 return
             }
      image = _embedText(image: &image, text: text) ?? image
      // Flip the image vertically
//            image = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: .downMirrored)
//    let resultImage = UIImage(cgImage: image)
    
    // save the image which contains the secret information to another image file
    if let pngData = image.pngData() {
      let cacheDirectory = try? FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
         let steganographyDirectory = cacheDirectory?.appendingPathComponent("Steganography")
         if !FileManager.default.fileExists(atPath: steganographyDirectory!.path) {
             try? FileManager.default.createDirectory(at: steganographyDirectory!, withIntermediateDirectories: true, attributes: nil)
         }
         let outputURL = steganographyDirectory?.appendingPathComponent("result.png")
        do {
           try pngData.write(to: outputURL!)
           resolve(outputURL?.absoluteString)
         } catch {
           reject("E_SAVE", "Error writing image to disk: \(error)", error)
         }
    }
    } else {
        reject("E_LOAD", "Failed to create file path from URL: \(imagePath)", nil)
        return
    }
        
     
      
  }
  
  func _embedText(image: inout UIImage, text: String,encoding: String.Encoding = .isoLatin1) -> UIImage? {
    guard let data = text.data(using: encoding) else { return nil } // Specify encoding
       let bytes = [UInt8](data)
       
       guard let inputCGImage = image.cgImage else { return nil }
       let width = inputCGImage.width
       let height = inputCGImage.height
       
       let colorSpace = CGColorSpaceCreateDeviceRGB()
       let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
       
       guard let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue) else { return nil }
       context.draw(inputCGImage, in: CGRect(x: 0, y: 0, width: width, height: height))
       
       var x = 0
       var y = 0
    var bitMask: UInt8 = 0x01 // Start with the most significant bit

    for byte in bytes {
            for _ in 0..<8 {
                // Calculate pixel index assuming row-major order (like Java's BufferedImage)
                let pixelIndex = y * width + x
                
                // Access pixel data using calculated index
                let pixelData = context.data!.assumingMemoryBound(to: UInt8.self) + pixelIndex * 4
                
                let blueComponentOffset = 2 // Assuming blue is the 3rd component (index 2)
                let currentColor = pixelData[blueComponentOffset]
                let newColor: UInt8

                if byte & bitMask > 0 {
                    newColor = currentColor | 0x01
                } else {
                    newColor = currentColor & 0xFE
                }

                pixelData[blueComponentOffset] = newColor
                bitMask <<= 1
                x += 1

                if x >= width {
                    x = 0
                    y += 1

                    if y >= height {
                        return nil // Image is full, handle this case
                    }
                }
            }
            bitMask = 0x01
        }
        
       guard let outputCGImage = context.makeImage() else { return nil }
       return UIImage(cgImage: outputCGImage)
        }
  private func setPixel(x: Int, y: Int, color: UInt32, in context: CGContext) {
          let pixelData = context.data?.assumingMemoryBound(to: UInt32.self)
          let pixelInfo: UInt32 = color
          pixelData?[y * context.bytesPerRow / 4 + x] = pixelInfo
      }
  
  @objc static func requiresMainQueueSetup() -> Bool {
      return true
    }
}
