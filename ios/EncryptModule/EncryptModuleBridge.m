//
//  EncryptModuleBridge.m
//  steganographyPoc
//
//  Created by Duy Anh Vu on 24/4/24.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(EncryptModule, NSObject)

RCT_EXTERN_METHOD(embedText:(NSString *)imagePath text:(NSString *)text resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
@end
