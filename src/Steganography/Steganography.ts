import {NativeModules, Platform} from 'react-native';
import {stringToArray} from 'utf8-to-bytes';
import varint from 'varint';
import LZUTF8 from 'lzutf8';

import {ImageNotEncodedError, MessageTooLongError} from './exceptions';
import {DecodeLSB, EncodeLSB} from './LSB';

const Bitmap = Platform.select({
  ios: NativeModules.Bitmap,
  android: NativeModules.Bitmap,
});
type AlgorithmNames = 'LSBv1';

interface IMetaData {
  [name: string]: number | string;
}

interface IAlgorithms {
  [name: number]: AlgorithmNames;
}

export default class Steganography {
  private readonly imageURI: string;
  private progress: {
    increment: number;
    value: number;
  };

  private numToAlgorithmTypes: IAlgorithms = {
    0: 'LSBv1',
  };

  constructor(imageURI: string) {
    this.imageURI = imageURI;
    this.progress = {
      increment: 0,
      value: 0,
    };
  }

  public async encode(
    message: string,
    algorithm: AlgorithmNames = 'LSBv1',
    metadata?: IMetaData,
  ) {
    try {
      var output = LZUTF8.compress(message);
      //[80, 84, 80, 77, 49]

      const binaryMessage = this.convertMessageToBits(output);
      //000001010101000001010100010100000100110100110001

      const imageData = await this.getImageData(
        this.imageURI,
        binaryMessage.length + 8,
      );
      // [131, 113, 89, 138, 120, 96, 145, 127, 103, 148, 130, 106, 153, 133, 109, 155, 135, 111, 156, 136, 112, 155, 135, 111, 158, 138, 114, 158, 138, 114, 159, 139, 115, 160, 140, 116, 163, 141, 118, 164, 142, 119, 164, 142, 119, 163, 141, 118, 162, 142, 118, 161, 141, 117, 161, 141, 117]
console.log('======imageData==============================');
console.log(imageData);
console.log('====================================');
      const newImageData = this.encodeData(
        imageData,
        binaryMessage,
        algorithm,
        metadata,
      );
      console.log('====newImageData================================');
      console.log(newImageData);
      console.log('====================================');
      // [130, 112, 88, 138, 120, 96, 144, 126, 102, 148, 130, 106, 152, 133, 108, 155, 134, 111, 156, 137, 112, 154, 134, 110, 158, 139, 114, 159, 138, 115, 158, 138, 114, 161, 140, 117, 162, 140, 118, 164, 142, 119, 164, 142, 119, 163, 140, 119, 162, 142, 119, 161, 140, 116, 160, 141, 117]

      const url = await this.getEncodedImageURI(newImageData);
      return {
        output,
        binaryMessage,
        imageData,
        newImageData,
        url,
      };
    } catch (error) {
      if (error instanceof RangeError) {
        throw new MessageTooLongError('Message too long to encode', message);
      } else {
        throw new Error(error);
      }
    }
  }

  public async decode(imageUrl: string) {
    try {
      const imageData = await this.getImageData(imageUrl, 200);
      //[130, 112, 88, 138, 120, 96, 144, 126, 102, 148, 130, 106, 152, 133, 108, 155, 134, 111, 156, 137, 112, 154, 134, 110, 158, 139, 114, 159, 138, 115, 158, 138, 114, 161, 140, 117, 162, 140, 118, 164, 142, 119, 164, 142, 119, 163, 140, 119, 162, 142, 119, 161, 140, 116, 160, 141, 117, 161, 141, 117, 162, 142, 118, 162, 142, 118, 161, 141, 117, 160, 140, 116, 161, 141, 117, 160, 140, 116, 160, 140, 116, 160, 140, 116, 160, 140, 116, 160, 140, 116, 160, 140, 116, 159, 139, 115, 160, 140, 116, 160, 140, 116, 160, 140, 116, 160, 140, 116, 160, 140, 116, 160, 140, 116, 159, 139, 115, 158, 138, 114, 159, 139, 115, 158, 138, 114, 158, 138, 114, 159, 139, 115, 161, 141, 117, 161, 141, 117, 159, 139, 115, 156, 136, 112, 156, 139, 113, 155, 138, 112, 156, 136, 111, 156, 136, 111, 155, 135, 110, 155, 135, 110, 155, 135, 110, 155, 135, 110, 154, 132, 108, 154, 132, 108, 154, 132, 108, 153, 131, 107, 153, 131, 107, 153, 131, 107, 153, 131, 107, 153, 131, 107, 153, 133, 108, 153, 133, 108, 154, 134, 109]

      const decodedDecimalData = this.decodeData(imageData);
      //[80, 84, 80, 77, 49]

      const message = LZUTF8.decompress(decodedDecimalData);
      // PTPM1
      return message;
    } catch (error) {
      if (error instanceof RangeError) {
        throw new ImageNotEncodedError(
          'Image is not encoded with any data',
          this.imageURI,
        );
      } else {
        throw new Error(error);
      }
    }
  }

  public updateProgress() {
    this.progress.value += this.progress.increment;
  }

  public getProgress() {
    return this.progress.value;
  }

  private convertMessageToBits(message: Uint8Array) {
    const messageLength = varint.encode(message.length);
    const arrayToEncode = [...messageLength, ...message];

    let bitsToEncode: string = '';
    for (const element of arrayToEncode) {
      bitsToEncode += this.convertDecimalToBytes(element as number);
    }

    return bitsToEncode;
  }

  private async getImageData(imageURI: string, length: number) {
    const pixels = await Bitmap.getPixels(imageURI, length);
    return pixels;
  }

  private encodeData(
    imageData: number[],
    data: string,
    algorithm: AlgorithmNames,
    metadata?: IMetaData,
  ) {
    this.progress.increment = 100 / data.length;
    metadata = this.setDefaultMetaData(
      metadata && algorithm !== 'LSBv1' ? metadata : {},
      algorithm,
    );
    const {newImageData, startEncodingAt} = this.encodeMetadata(
      imageData,
      metadata,
    );

    let encodedData;
    switch (algorithm) {
      default: {
        encodedData = new EncodeLSB(this.updateProgress.bind(this)).encode(
          newImageData,
          data,
          startEncodingAt,
        );
      }
    }

    return encodedData;
  }

  private setDefaultMetaData(metadata: IMetaData, algorithm: AlgorithmNames) {
    const algorithmNum = this.algorithmsTypesToNum(algorithm);
    metadata.algorithm = algorithmNum;

    return metadata;
  }

  private algorithmsTypesToNum(algorithm = 'LSBv1') {
    const values = Object.values(this.numToAlgorithmTypes);
    return values.indexOf(algorithm);
  }

  private encodeMetadata(imageData: number[], metadata: IMetaData) {
    const metadataToEncode = [];
    const dataToEncode = ['algorithm'];

    for (const data of dataToEncode) {
      const tempData = metadata[data];
      if (!tempData && tempData !== 0) {
        continue;
      }

      let decimalDataToEncode;
      if (typeof tempData === 'string') {
        decimalDataToEncode = stringToArray(tempData) as number[];
      } else {
        decimalDataToEncode = varint.encode(tempData);
      }
      metadataToEncode.push(...decimalDataToEncode);
    }

    let binaryMetadata = '';
    for (const data of metadataToEncode) {
      binaryMetadata += this.convertDecimalToBytes(data);
    }
    const imageDataWithMetadata = new EncodeLSB().encode(
      imageData,
      binaryMetadata,
    );

    return {
      newImageData: imageDataWithMetadata,
      startEncodingAt: binaryMetadata.length,
    };
  }

  private async getEncodedImageURI(data: number[]) {
    const uri = await Bitmap.setPixels(this.imageURI, data);
    return uri;
  }

  private decodeData(imageData: number[]) {
    const {algorithm, startDecodingAt} = this.decodeMetadata(imageData);
    let decodedMessage;

    switch (algorithm) {
      default: {
        decodedMessage = new DecodeLSB(this.updateProgress.bind(this)).decode(
          imageData,
          startDecodingAt,
        );
      }
    }

    const decodedDecimal: number[] = [];
    for (const byte of decodedMessage) {
      const decimal = this.convertBytesToDecimal(byte);
      decodedDecimal.push(decimal);
    }

    return new Uint8Array(decodedDecimal);
  }

  private decodeMetadata(imageData: number[]) {
    const decodeLSB = new DecodeLSB();
    const algorithmTypeBinary = decodeLSB.decodeNextByte(imageData);
    const algorithmNum = this.convertBytesToDecimal(algorithmTypeBinary);
    const algorithm = this.numToAlgorithmTypes[algorithmNum] || 'LSBv1';
    const metadata: IMetaData = {};

    const startDecodingAt = decodeLSB.getCurrentIndex();
    return {
      algorithm,
      metadata,
      startDecodingAt,
    };
  }

  private convertDecimalToBytes(data: number) {
    const binaryString = data.toString(2);
    const nearestByte = Math.ceil(binaryString.length / 8) * 8;
    const byte = binaryString.padStart(nearestByte, '0');
    return byte;
  }

  private convertBytesToDecimal(bytes: string) {
    const decimal = parseInt(bytes, 2);
    return decimal;
  }
}
