/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useState} from 'react';
import type {PropsWithChildren} from 'react';
import {
  Image,
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {launchImageLibrary} from 'react-native-image-picker';
import Steganography from './src/Steganography/Steganography';
import RNFS from 'react-native-fs';
import {NativeModules} from 'react-native';

type SectionProps = PropsWithChildren<{
  title: string;
}>;

function Section({children, title}: SectionProps): React.JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
}

function App(): React.JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [showObject, setShowObject] = useState({
    encodedMessage: 'PTPM1',
    decodedMessage: '',
    lzUTF8EncodedOutput: '',
    encodeBinaryMessage: '',
    encodedImageData: '',
    encodedNewImageData: '',
    encodedUri: '',
    originalUri: '',
  });

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        {/* <Header /> */}
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
            rowGap: 15,
          }}>
          <TouchableOpacity
            onPress={async () => {
              launchImageLibrary(
                {selectionLimit: 1, mediaType: 'mixed'},
                async response => {
                  if (response && response.assets) {
                    const {originalPath, uri} = response.assets[0];
                    if (Platform.OS === 'android') {
                      if (!originalPath) return;
                      console.log(uri);
                      const result = await NativeModules.Bitmap.convert(
                        originalPath,
                        'asdf',
                      );
                      console.log('=====result===============================');
                      console.log(result);
                      console.log('====================================');
                    } else {
                      if (!uri) return;
                      console.log(uri);

                      NativeModules.EncryptModule.embedText(uri, 'Secret')
                        .then(result => {
                          console.log('====================================');
                          console.log(result);
                          console.log('====================================');
                        })
                        .catch(error => {
                          console.log('====================================');
                          console.log(error);
                          console.log('====================================');
                        });
                    }
                    // let steganography = new Steganography(uri);
                    // const encodedImage = await steganography.encode(
                    //   showObject.encodedMessage,
                    // );
                    // setShowObject(prev => ({
                    //   ...prev,
                    //   lzUTF8EncodedOutput: encodedImage.output,
                    //   encodeBinaryMessage: encodedImage.binaryMessage,
                    //   encodedImageData: encodedImage.imageData,
                    //   encodedNewImageData: encodedImage.newImageData,
                    //   originalUri: uri,
                    //   encodedUri: encodedImage.url,
                    // }));
                    // steganography = new Steganography(encodedImage.url);
                    // const decodedMessage = await steganography.decode(encodedImage.url);
                    // uriToBase64(encodedImage)
                    //   .then(base64 => {
                    //     if (base64) saveLogFromString(base64, 'log');
                    //     // Use the base64 string as needed (e.g., set it as state, send to server, etc.)
                    //   })
                    //   .catch(error => {
                    //     console.error(
                    //       'Error converting image to base64:',
                    //       error,
                    //     );
                    //   });
                  }
                },
              );
            }}>
            <View style={{backgroundColor: 'red', padding: 50}}>
              <Text style={{fontSize: 50}}>Chọn ảnh</Text>
            </View>
          </TouchableOpacity>

          <View style={{paddingHorizontal: 15, rowGap: 20}}>
            <View>
              <Text style={styles.label}>Khoá:</Text>
              <TextInput
                style={{
                  height: 40,
                  borderColor: 'gray',
                  borderWidth: 2,
                  borderRadius: 30,
                  paddingHorizontal: 15,
                  fontSize: 30,
                }}
                onChangeText={value =>
                  setShowObject(prev => ({...prev, encodedMessage: value}))
                }
                value={showObject.encodedMessage}
              />
            </View>
            <View>
              <Text style={styles.label}>
                var output = LZUTF8.compress(message)
              </Text>
              <TextInput
                style={{
                  height: 40,
                  borderColor: 'gray',
                  borderWidth: 2,
                  borderRadius: 30,
                  paddingHorizontal: 15,
                  fontSize: 30,
                }}
                editable={true}
                value={showObject.lzUTF8EncodedOutput.toString()}
              />
            </View>

            <View>
              <Text style={styles.label}>
                const binaryMessage = this.convertMessageToBits(output);
              </Text>
              <TextInput
                style={{
                  height: 40,
                  borderColor: 'gray',
                  borderWidth: 2,
                  borderRadius: 30,
                  paddingHorizontal: 15,
                  fontSize: 30,
                }}
                editable={true}
                value={showObject.lzUTF8EncodedOutput.toString()}
              />
            </View>
            <View>
              <Text style={styles.label}>
                const binaryMessage = this.convertMessageToBits(output);
              </Text>
              <TextInput
                style={{
                  height: 150,
                  borderColor: 'gray',
                  borderWidth: 2,
                  borderRadius: 30,
                  paddingHorizontal: 15,
                  fontSize: 30,
                }}
                multiline={true}
                numberOfLines={5}
                editable={true}
                value={showObject.encodeBinaryMessage}
              />
            </View>

            <View>
              <Text style={styles.label}>Ảnh trước khi encode:</Text>
              {showObject.originalUri.length > 0 && (
                <Image
                  style={{width: 300, aspectRatio: 1, resizeMode: 'contain'}}
                  source={{
                    uri: showObject.originalUri,
                  }}
                />
              )}
              <TextInput
                style={{
                  height: 150,
                  borderColor: 'gray',
                  borderWidth: 2,
                  borderRadius: 30,
                  paddingHorizontal: 15,
                  fontSize: 30,
                }}
                multiline={true}
                numberOfLines={5}
                editable={true}
                value={showObject.originalUri}
              />
            </View>
            <View>
              <Text style={styles.label}>Ảnh sau khi encode:</Text>
              {showObject.originalUri.length > 0 && (
                <Image
                  style={{width: 300, aspectRatio: 1, resizeMode: 'contain'}}
                  source={{
                    uri: showObject.encodedUri,
                  }}
                />
              )}
              <TextInput
                style={{
                  height: 150,
                  borderColor: 'gray',
                  borderWidth: 2,
                  borderRadius: 30,
                  paddingHorizontal: 15,
                  fontSize: 30,
                }}
                multiline={true}
                numberOfLines={5}
                editable={true}
                value={showObject.encodedImageData.toString()}
              />
              <TextInput
                style={{
                  height: 150,
                  borderColor: 'gray',
                  borderWidth: 2,
                  borderRadius: 30,
                  paddingHorizontal: 15,
                  fontSize: 30,
                }}
                multiline={true}
                numberOfLines={5}
                editable={true}
                value={showObject.encodedNewImageData.toString()}
              />
              <TextInput
                style={{
                  height: 150,
                  borderColor: 'gray',
                  borderWidth: 2,
                  borderRadius: 30,
                  paddingHorizontal: 15,
                  fontSize: 30,
                }}
                multiline={true}
                numberOfLines={5}
                editable={true}
                value={showObject.encodedUri}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const uriToBase64 = async (uri: string) => {
  try {
    const base64 = await RNFS.readFile(uri, 'base64');
    return base64;
  } catch (error) {
    console.error('Error converting to Base64:', error);
    // Handle error appropriately, e.g., show an error message to the user
  }
};
const saveLogFromString = async (logString: string, fileName: string) => {
  // Define the file path where you want to save the log
  const filePath = RNFS.DownloadDirectoryPath + `/${fileName}.txt`;

  try {
    await RNFS.writeFile(filePath, logString, 'utf8');
    console.log(`Log saved to: ${filePath}`);
  } catch (error) {
    console.error('Error saving log:', error);
    // Handle error appropriately
  }
};
const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  label: {
    fontWeight: '600',
    fontSize: 18,
  },
});

export default App;
